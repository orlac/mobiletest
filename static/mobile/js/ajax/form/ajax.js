/*var ajaxEvent = {
    eventPreload : 'eventPreload',
    eventLoad : 'eventLoad'
}*/

var AjaxForm = function()
{   
    this.preload = function()
    {
	jQuery(document).trigger(ajaxEvent.eventPreload);
    }
    
    this.onload = function()
    {
	jQuery(document).trigger(ajaxEvent.eventLoad);
    }
    
    this.onSuccess = function(data)
    {
	console.log('success: '+ data.success);
    }
    
    this.onError = function(data)
    {
	console.log('success: false');
	console.log(data.errors);
    }
    
    this.init = function( element, url )
    {
	
	var preload = this.preload;
	var onload = this.onload;
	var onSuccess = this.onSuccess;
	var onError = this.onError;
	if(typeof url == 'undefined')
	{
	    var url = jQuery(element).attr('action');
	    url += '?ajax=true';
	}
	
	jQuery(element).submit(function()
	{
	    preload();
	    console.log('url '+url);
	    jQuery(this).ajaxSubmit(
	    {
		dataType : 'json',
		url : url,
		success: function(data)
		{
		    console.log(data);
		    if(typeof data.success != 'undefined' && data.success == true)
		    {
			onSuccess(data);
			onload();
		    }else if( typeof data.errors != 'undefined' )
		    {
			data = getError(data);
			onError(data)
			onload();
		    }
		},
		error: function(error, text)
		{
		   console.log(error);
		   data = getError(data);
		   onError(data)
		   onload();
		}
	    });
	    return false;
	})
    }
    
    function getError(data)
    {
	if( typeof data.errors != 'object' && typeof data.errors != 'array')
	{
	    data.errors = [data.errors];
	}
	return data;
    }
}