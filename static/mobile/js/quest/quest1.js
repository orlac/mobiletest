var Quest1 = function()
{
    var me = this;
    
    this.ini = function()
    {
        jQuery('.draggable').unbind("dragstart");
        jQuery('.draggable').draggable({
	    
	    //если перетащили не туда, куда нужно
	    stop: function( event, ui ){	
	        var parent = jQuery(this).parent();
		var base = jQuery(parent).attr('var:base');
		var _static = jQuery(parent).attr('var:static');
		jQuery(this).css('top', '0');
		jQuery(this).css('left', '0');
		if(base && !_static){    
		    jQuery(this).appendTo(jQuery('.base'));    
		}
	    }
	});
	jQuery('.draggable').bind("dragstart", function(event, ui){
            event.stopPropagation();
        });
        jQuery('.droppable').droppable({
            
	    drop: function( event, ui ){
                ui.draggable.css('top', '0');
		ui.draggable.css('left', '0');
		
		var clonable = jQuery(this).attr('var:clone');
		//base
		var base = jQuery(this).attr('var:base');
		//static
		var _static = jQuery(this).attr('var:static');
		var drag_id = jQuery(ui.draggable).attr('var:id');
		//уже присоединен
		var is_joint = jQuery(ui.draggable).attr('var:joint');
		//такой елемент уже есть
		var already_isset = false;
		jQuery(this).find('.draggable').each(function(key, obj){
		    //если такой елемент уже есть
		    if( jQuery(obj).attr('var:id') == drag_id ){
			already_isset = true;
		    }
		})
		me.ini();
		if(_static && already_isset){
		    if(!is_joint){
			//возвращаем на базу
			jQuery(ui.draggable).attr('var:joint', '');
			jQuery(ui.draggable).appendTo(jQuery('.base'));
			jQuery(ui.draggable).draggable();
		    }else{
			//jQuery(ui.draggable).remove();
		    }
		    return;
		}
		if(base){
		    if(is_joint){
			//jQuery(ui.draggable).remove();
			jQuery(ui.draggable).appendTo(jQuery('.base'));
			jQuery(ui.draggable).draggable();
		    }
		}
		if(clonable){
		    //var clone = ui.draggable//.clone()
		    jQuery(this).append(ui.draggable);
		    jQuery(ui.draggable).draggable();
		    jQuery(ui.draggable).attr('var:joint', true);
		}
            }
        });
    }
    
    this.sendAnswer = function(form){
	var _static = jQuery(form).find('.static');
	var send_data = {};
	jQuery(_static).find('.droppable').each(function(key, obj){
	    send_data[jQuery(obj).attr('var:id')] = [];
	    var _id = jQuery(obj).attr('var:id');
	    jQuery(obj).find('.draggable').each(function(_key, _obj){
		send_data[_id].push(jQuery(_obj).attr('var:id'));
	    })
	});
	//data;
	jQuery('#div_result').html('...');
	ajax.query({
	    url: jQuery(form).attr('action'),
	    type: 'get',
	    data: {
		data: send_data,
		ajax: true
	    },
	    success: function(_data){
		jQuery('#div_result').html(_data.message);
		//alert(_data.message);
	    },
	    error: function(_data){
		jQuery('#div_result').html(_data.errors.join('</br>'));
		//alert(_data.errors.join('\n'));
	    }
	});
    }
}

jQuery(document).bind('pageinit', function()
{
    //jQuery('body').addClass('droppable');
    var quest = new Quest1();
    quest.ini();
    jQuery('#form_quest1').submit(function()
    {
	quest.sendAnswer(this);
	return false;
    })
})