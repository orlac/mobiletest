var Quest2 = function()
{
    var me = this;
    
    this.items = {}
    
    this.ini = function()
    {
        jQuery.ajax({
		type: "GET", 
		url: "/static/quest/part2.xml", 
		dataType: "xml", 
		success: function(xml) { 
		    _items = jQuery.xml2json(xml, true /* extended structure */);
		    me.items = _items.items.shift().piece;
		    console.log(me.items);
		    me.render(me.items);
		}
	    });
    }
    
    this.render = function(items){
	
	/**
	<div class="radio-button" id="button_0">
		<input type="radio" class="niceCheck" id="lbl1" var:id="0" />
		<label for="lbl1">Уровень модернизации, время производственного цикла, себестоимость, <br />прибыль</label>
		<div style="clear:both"></div>
	  </div>
	*/
	var _is = [];
	for(var i in items){
	    var _div =  jQuery('<div/>').attr('id', 'button_'+items[i].id).addClass('radio-button');
	    var _input = jQuery('<input/>').addClass('niceCheck')
		.attr('type', 'radio').attr('id', 'lbl'+items[i].id).attr('name', items[i].id );
	    var _label = jQuery('<label/>')./*prop('for', 'lbl'+items[i].id).*/html(items[i].title ).click(function(){
		//jQuery('#'+jQuery(this).attr('for')).click();
		//jQuery('#'+jQuery(this).attr('for')).change();
		//jQuery(this).mousedown();
		
		//_input.click();
	    });
	    var _both = jQuery('<div/>').attr('style', 'clear:both');
	    _div.append( _input );
	    _div.append( _label );
	    _div.append( _both );
	    jQuery('#div_item_list').append(_div);
	}
	
	var _n = new NiceCheck().ini();
	jQuery('#form_part_2 .niceCheck').click(function(){
	    
	    changed = [];
	    jQuery('#form_part_2').find('input').each(function(key, obj){
		if(jQuery(obj).attr('checked')){
		    changed.push(1);    
		}
	    });
	    if( changed.length > 0 ){
		jQuery('#verify_button').addClass('ready');
		jQuery('#verify_button').removeAttr('disabled');
	    }else{
		jQuery('#verify_button').removeClass('ready');
		jQuery('#verify_button').attr('disabled', true);
	    }
	}) ;
	
    }
    
    this.sendAnswer = function(form){
	
	//если уже продолжить
	var complete = jQuery('#verify_button').attr('complete');
	if(complete){
	    return false;
	}
	
	var correct = [];
	var incorrect = [];
	jQuery(form).find('.niceCheck input').each(function(key, obj){
	    var item_id = jQuery(obj).prop('name')
	    var checked = jQuery(obj).attr('checked')
	    for(var i in me.items){
		if(me.items[i].id == item_id){
		    
		    var _true =  ( me.items[i].true )? 1 : 0 ;
		    var _send = ( checked == 'checked')? 1 : 0 ;
		    if(_true == _send){
			correct.push(item_id)
		    }else{
			incorrect.push(item_id)
		    }
		}
	    }
	});
	if(incorrect.length > 0){
	    jQuery('#result_block').addClass('wrong');
	    jQuery('#result_block .inner-text').html('Ответ не верный!');
	    jQuery('#verify_button').addClass('wrong');
	    jQuery('#verify_button').removeAttr('complete');
	    jQuery('#verify_button').removeClass('correct');
	}else{
	    jQuery('#result_block').addClass('correct');
	    jQuery('#result_block .inner-text').html('Вы ответили правильно!');
	    jQuery('#verify_button').removeClass('wrong');
	    jQuery('#verify_button').addClass('correct');
	    jQuery('#verify_button').attr('complete', true);
	}
	me.showIncorrect(correct, incorrect);
    }
    
    /**
     * подсведка неправильных ответов
    */
    this.showIncorrect = function(correct, incorrect){
	
	jQuery('.radio-button').removeClass('correct');
	jQuery('.radio-button').removeClass('wrong');
	for(var i in correct){
	    jQuery('#button_'+correct[i]).addClass('correct');
	}
	for(var i in incorrect){
	    jQuery('#button_'+incorrect[i]).addClass('wrong');
	}
	
	setTimeout( function(){
		jQuery('.radio-button').removeClass('correct');
		jQuery('.radio-button').removeClass('wrong');
	}, 2000 );
	
	/*var _static = jQuery('.static');
	jQuery(_static).find('.item').each(function(key, obj){
	    for(var i in ids){
		if(ids[i] == jQuery(obj).attr('var:id')){
		    jQuery(obj).addClass('incorrect');
		    setTimeout( function(){
			jQuery(obj).removeClass('incorrect');
		    }, 1000 );
		}
	    }
	});*/
    }
}


jQuery(document).bind('pageinit', function()
{
    //jQuery('body').addClass('droppable');
    var _quest = new Quest2();
    _quest.ini();
    jQuery('#form_part_2').submit(function()
    {
	_quest.sendAnswer(this);
	return false;
    })    
})