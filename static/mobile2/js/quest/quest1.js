var Quest1 = function()
{
    var me = this;
    
    this.ini = function()
    {
        //alert(123);
	jQuery('.draggable').unbind("dragstart");
        jQuery('.draggable').draggable({
	    
	    //если перетащили не туда, куда нужно
	    stop: function( event, ui ){	
	        var parent = jQuery(this).parent();
		var base = jQuery(parent).attr('var:base');
		var _static = jQuery(parent).attr('var:static');
		jQuery(this).css('top', '0');
		jQuery(this).css('left', '0');
		if(base && !_static){    
		    //jQuery(this).appendTo(jQuery('.base'));
		    me.backElementToBase(this);
		}
		/**
		 * делаем кнопку активной, если перетащили все
		*/
		me.toggleButtonActive();
	    }
	});
	jQuery('.draggable').bind("dragstart", function(event, ui){
            event.stopPropagation();
        });
        jQuery('.droppable').droppable({
            
	    drop: function( event, ui ){
                
		ui.draggable.css('top', '0');
		ui.draggable.css('left', '0');
		
		var clonable = jQuery(this).attr('var:clone');
		//base
		var base = jQuery(this).attr('var:base');
		//static
		var _static = jQuery(this).attr('var:static');
		var drag_id = jQuery(ui.draggable).attr('var:id');
		//уже присоединен
		var is_joint = jQuery(ui.draggable).attr('var:joint');
		//такой елемент уже есть
		var already_isset = false;
		jQuery(this).find('.draggable').each(function(key, obj){
		    //если такой елемент уже есть
		    if( jQuery(obj).attr('var:id') == drag_id ){
			already_isset = true;
		    }
		})
		me.ini();
		if(_static && already_isset){
		    if(!is_joint){
			//возвращаем на базу
			jQuery(ui.draggable).attr('var:joint', '');
			//jQuery(ui.draggable).appendTo(jQuery('.base'));
			me.backElementToBase(ui.draggable);
			jQuery(ui.draggable).draggable();
		    }else{
			//jQuery(ui.draggable).remove();
		    }
		    return;
		}
		if(base){
		    if(is_joint){
			//jQuery(ui.draggable).remove();
			//jQuery(ui.draggable).appendTo(jQuery('.base'));
			me.backElementToBase(ui.draggable);
			jQuery(ui.draggable).draggable();
		    }
		}
		if(clonable){
		    //var clone = ui.draggable//.clone()
		    jQuery(this).append(ui.draggable);
		    jQuery(ui.draggable).draggable();
		    jQuery(ui.draggable).attr('var:joint', true);
		}
            }
        });
    }
    
    /**
     * вернуть элемент на базу
    */
    this.backElementToBase = function(item){
	var finded = false;
	jQuery('.base').find('div').each(function(key, obj){
	    if(!finded){
		if( jQuery(obj).attr('var:id') == jQuery(item).attr('var:id') ){
		    jQuery(item).appendTo(obj);
		    finded = true;
		}
	    }
	})
    }
    
    this.toggleButtonActive = function(){
	var count = 0;
	jQuery('.base').find('.item').each( function( key, obj ){
	    count++;
	} );
	if(count > 0){
	    jQuery('#button_send_answer').removeClass('active');
	    jQuery('#button_send_answer').attr('disabled', 'disabled');
	}else{
	    jQuery('#button_send_answer').addClass('active');
	    jQuery('#button_send_answer').removeAttr('disabled');
	}
    }
    
    this.checkDataByXml = function(data){
	jQuery(document).ready(function(){
	    jQuery.ajax({
		type: "GET", 
		url: "/static/quest/part1.xml", 
		dataType: "xml", 
		success: function(xml) { 
		    validate_data = jQuery.xml2json(xml, true /* extended structure */);
		    validate_data = validate_data.items.shift().piece;
		    
		    console.log(validate_data);
		    console.log(data);
		    
		    var _items = {};
		    while( validate_data.length > 0 ){
			var piece = validate_data.shift();
			if( !_items[piece.static] ){
			    _items[piece.static] = []
			}
			_items[piece.static].push( piece.id );
		    }
		    
		    console.log(_items)
		    var incorrect = [];
		    for (var _static_id in _items ){
			var _static = _items[_static_id];
			var send_static = data[_static_id];
			var _i = [];
			
			_i = jQuery.grep(_static,function (item) {
			    return jQuery.inArray(item, send_static) < 0;
			});
			console.log(_i);
			incorrect = incorrect.concat(_i);
		    }
		    console.log(incorrect);
		    me.showIncorrect(incorrect);
		    if ( incorrect.length > 0 ){
			jQuery('.result-block').removeClass('accepted');
			jQuery('.result-block').addClass('wrong');
			jQuery('#div_result').html('<span class="brown-text">Попробуйте еще раз!</span>');
		    }else{
			jQuery('.result-block').addClass('accepted');
			jQuery('.result-block').removeClass('wrong');
			jQuery('#button_send_answer').removeClass('active');
			jQuery('#button_send_answer').attr('disabled', 'disabled');
			jQuery('#div_result').html('<span class="brown-text">Вы выполнили <br />задание! <br />Можете перейти <br />к следующему разделу</span>');
		    }
		    /*
		    var incorrect = [];
		    while( validate_data.length > 0 ){
			var piece = validate_data.shift();
			for(var i in data){
			    if( Number(i) == Number(piece.static) ){
				for(var j in data[i]){
				    var answer = data[i][j]
				    if( Number(answer) == Number(piece.id) ){
					delete(data[i][j]);
				    }
				}
			    }
			    
			}
		    }
		    console.log(data)*/
		    
		}
	    });
		
	});
    }
    
    this.sendAnswer = function(form){
	var _static = jQuery(form).find('.static');
	var send_data = {};
	jQuery(_static).find('.droppable').each(function(key, obj){
	    send_data[jQuery(obj).attr('var:id')] = [];
	    var _id = jQuery(obj).attr('var:id');
	    jQuery(obj).find('.draggable').each(function(_key, _obj){
		send_data[_id].push(jQuery(_obj).attr('var:id'));
	    })
	});
	
	//data;
	me.checkDataByXml(send_data)
	/*jQuery('#div_result').html('...');
	ajax.query({
	    url: jQuery(form).attr('action'),
	    type: 'get',
	    data: {
		data: send_data,
		ajax: true
	    },
	    success: function(_data){
		jQuery('.result-block').addClass('accepted');
		jQuery('.result-block').removeClass('wrong');
		jQuery('#button_send_answer').removeClass('active');
		jQuery('#button_send_answer').attr('disabled', 'disabled');
		jQuery('#div_result').html(_data.message);
		//alert(_data.message);
	    },
	    error: function(_data){
		jQuery('.result-block').removeClass('accepted');
		jQuery('.result-block').addClass('wrong');
		
		jQuery('#div_result').html(_data.errors.join('</br>'));
		//me.showIncorrect(_data.inc_answers);
		//alert(_data.errors.join('\n'));
	    }
	});*/
    }
    
    /**
     * подсведка неправильных ответов
    */
    this.showIncorrect = function(ids){
	var _static = jQuery('.static');
	jQuery(_static).find('.item').each(function(key, obj){
	    for(var i in ids){
		if(ids[i] == jQuery(obj).attr('var:id')){
		    jQuery(obj).addClass('incorrect');
		    setTimeout( function(){
			jQuery(obj).removeClass('incorrect');
		    }, 1000 );
		}
	    }
	});
    }
}


jQuery(document).bind('pageinit', function()
{
    //jQuery('body').addClass('droppable');
    var _quest = new Quest1();
    _quest.ini();
    jQuery('#form_quest1').submit(function()
    {
	_quest.sendAnswer(this);
	return false;
    })    
})