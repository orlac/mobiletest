<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.2.4 or newer
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Academic Free License version 3.0
 *
 * This source file is subject to the Academic Free License (AFL 3.0) that is
 * bundled with this package in the files license_afl.txt / license_afl.rst.
 * It is also available through the world wide web at this URL:
 * http://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to obtain it
 * through the world wide web, please send an email to
 * licensing@ellislab.com so we can send you a copy immediately.
 *
 * @package		CodeIgniter
 * @author		EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2012, EllisLab, Inc. (http://ellislab.com/)
 * @license		http://opensource.org/licenses/AFL-3.0 Academic Free License (AFL 3.0)
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

class Part extends CI_Controller {
    
        
        protected $_static = array('Фредерик Тейлор', 'Сакити Тойода', 'Генри Форд', 'Альфред Слоун', 'Кийитиро Тойода', 'Тайити Оно', 'Алексей Гастев');
        protected $_items = array('«4 принципа научного менеджмента»',
                                 'Принцип автономизации',
                                 'Стандартизация деталей и механизмов',
                                 'Стандартизация операций рабочих',
                                 'Окончательное формирование системы массового производства',
                                 'Идея производства «точно вовремя»',
                                 'Первые шаги в формировании TPS',
                                 'вовлечение людей',
                                 'научная организация труда');
        /*protected $_items = array('Идея производства «точно вовремя»',
                                  'Стандартизация деталей и механизмов',
                                  'Окончательное формирование системы массового производства',
                                  '«4 принципа научного менеджмента»',
                                  'Принцип автономизации',
                                  'научная организация труда',
                                  'Стандартизация операций рабочих',
                                  'Первые шаги в формировании TPS',
                                  'вовлечение людей',
                                 );*/
        /*protected $answers = array( 0 => array(3),
                                    1 => array(4),
                                    2 => array(1, 6),
                                    3 => array(2),
                                    4 => array(0,7),
                                    5 => array(8),
                                    6 => array(5));*/
        
        protected $answers = array( 0 => array(0),
                                    1 => array(1),
                                    2 => array(2, 3),
                                    3 => array(4),
                                    4 => array(5,6),
                                    5 => array(7),
                                    6 => array(8));
        
        /**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
            $this->load->library('Layout', array( 'controller' => $this, 'layout' => 'part1' ) );
            $this->layout->scripts[] = 'jquery-ui-touch-punch/jquery.ui.touch-punch.js';
            $this->layout->scripts[] = 'quest/quest1.js';
            $this->layout->scripts[] = 'ajax/load.js';
	    $this->layout->scripts[] = 'jquery.xml2json.js';
            $this->layout->view(__CLASS__.'/'.__FUNCTION__, array('static' => $this->_static, 'items' => $this->_items)); 
	}
        
        public function answer()
        {
            
            $data = (array)$this->input->get('data');
            //array_multisort($data, SORT_ASC, SORT_STRING);
            //array_multisort($this->answers, SORT_ASC, SORT_STRING);
            $answers = $this->answers;
            $incorrect_answers = array();
            if( count($data) > 0 )
            {
                foreach($data as $key => $arr_in)
                {
                    if( isset($answers[$key]) )
                    {
                        sort($arr_in);
                        sort($answers[$key]);
                        $diff = array_diff($arr_in, $answers[$key]);
                        if(count($diff) === 0)
                        //if($arr_in == $answers[$key])
                        {
                            unset($answers[$key]);
                        }else{
                            $incorrect_answers = array_merge($incorrect_answers, $diff );
                        }
                    }
                }
            }
            if(count( $answers ) == 0)
            {
                $_data = array('success' => true, 'data' => array('message' => '<span class="brown-text">Вы выполнили <br />задание! <br />Можете перейти <br />к следующему разделу</span>'));
            }else
            {
                $_data = array('success' => false, 'errors' => array('<span class="brown-text">Попробуйте еще раз!</span>'), 'inc_answers' => $incorrect_answers);
            }
            echo json_encode($_data);exit;
        }
        
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */