<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta content="" name="keywords" />
	<meta content="" name="discription" />
	<title><?echo implode(' ', $title);?></title> 
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
	<script src="http://code.jquery.com/mobile/1.1.0/jquery.mobile-1.1.0.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.7/jquery-ui.min.js"></script>
	<script src="<? echo $static_url; ?>js/niceCheckbox.js" type="text/javascript"></script>
	<script src="<? echo $static_url; ?>js/browsers.js" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="<? echo $static_url; ?>css/part2.css" />
	<?
	while($style = array_shift($styles)){
	    ?>
	    <link rel="stylesheet" href="<? echo $style; ?>" type="text/css" />
	    <?
	}
	?>
	<?
	while($script = array_shift($scripts)){
	    ?>
	    <script src="<? echo $script; ?>" ></script>
	    <?
	}
	?>
	<style>
	.ui-loader{
		display: none;
	}	
	</style>
</head>
<body class="gray-bg">
        
          <!-- site container -->
          <div id="container">
	        <div class="gray-line">
		      <div class="top-title-block gray">
			    <h1>Основы Бережливого производства</h1>
		      </div>
	        </div>
	        
	        <!-- cont -->
	        <div id="cont" class="small">
		      
		      <!-- marking-block -->
		      <div class="marking-block">
			    <?php echo $content_for_layout?>
		      </div>
		      <!-- end marking-block -->
		      
	        </div>
	        <!-- end cont -->
	        
          </div>
          <!-- end site container -->
          
</body>
</html>