<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html> 
	<head> 
	<title><?echo implode(' ', $title);?></title> 
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
	<script src="http://code.jquery.com/mobile/1.1.0/jquery.mobile-1.1.0.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.7/jquery-ui.min.js"></script>
	<script src="<? echo $static_url; ?>js/browsers.js" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="<? echo $static_url; ?>css/part1.css" />
    <?
    while($style = array_shift($styles)){
        ?>
        <link rel="stylesheet" href="<? echo $style; ?>" type="text/css" />
        <?
    }
    ?>
    <?
    while($script = array_shift($scripts)){
        ?>
        <script src="<? echo $script; ?>" ></script>
        <?
    }
    ?>
<style>
.ui-loader{
	display: none;
}	
</style>    
</head> 
<body> 

<div data-role="page" id="container">
	<!-- cont -->
	<div id="cont">
		<?php echo $content_for_layout?>	
	</div>
</div>

</body>

</html>