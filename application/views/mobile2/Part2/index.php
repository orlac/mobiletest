<div class="quest-block green">
	<div class="green-number">
	      1
	</div>
	<div class="text-box">
	      На улучшение каких параметров непосредственно влияет внедрение 
	      <br />бережливого производства (что такое треугольник эффективности)?
	</div>
	<div style="clear:both"></div>
</div>
  <form id="form_part_2">
	<div class="answer-block" id="div_item_list">
	      <!--
	      <div class="radio-button" id="button_0">
		    <input type="radio" class="niceCheck" id="lbl1" var:="" />
		    <label for="lbl1">Уровень модернизации, время производственного цикла, себестоимость, <br />прибыль</label>
		    <div style="clear:both"></div>
	      </div>
	      <div class="radio-button wrong" id="button_1">
		    <input type="radio" class="niceCheck" id="lbl2" />
		    <label for="lbl2">Безопасность, качество, время производственного цикла, себестоимость</label>
		    <div style="clear:both"></div>
	      </div>
	      <div class="radio-button correct" id="button_2">
		    <input type="radio" class="niceCheck" id="lbl3" />
		    <label for="lbl3">Cебестоимость, доля рынка, объем производства</label>
		    <div style="clear:both"></div>
	      </div>
	      <div class="radio-button" id="button_3">
		    <input type="radio" class="niceCheck" id="lbl4" />
		    <label for="lbl4">Прибыль компании, качество, удовлетворенность клиентов</label>
		    <div style="clear:both"></div>
	      </div>
	      -->
	</div>
	<div class="total-block">
	      <div class="result-block" id="result_block">
		    <div class="inner-text">
			      Выберите правильный вариант ответа!
		    </div>
	      </div>
	      <div class="button-block">
		    <button disabled="true" id="verify_button" class="verify-button" role="button"> </button>
	      </div>
	      <div style="clear:both"></div>
	</div>
  </form>