<div class="top-title-block">
	<h1>История развития производственных систем</h1>
</div>
<div class="quest-block">
	В чем заключался вклад этих людей
	<br />в развитие принципов Бережливого производства?
</div>
<div class="answer-block">
<form id="form_quest1" action="/welcome/answer">
	<div class="answer-inner-block static">
		<div class="line-block">
		<?
		$i = 0;
		while($stat = array_shift($static))
		{
			?>
			<div class="droppable"
				var:id="<? echo $i ?>"
				var:clone="1"
				var:static="1">
				<div class="name-box">
				      <? echo $stat; ?>
				</div>
				<div class="top-bg">
				</div>
				<div class="bottom-bg">
				</div>
			</div>
			<?
			$i++;
			if($i == 4){
				?>
			<div style="clear:both"></div>
		</div>
		<div class="line-block last">	
				<?
			}
			
		}
		?>
			<div class="result-block">
				<button role="button" id="button_send_answer"
					disabled="disabled"
					class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
					<span class="ui-button-text">Проверить</span>
				</button>
				<div id="div_result">
					<p>Распределите <br />элементы по ячейкам*</p>
					<span>*может быть несколько <br />вариантов ответа</span>
				</div>
			</div>
			<div style="clear:both"></div>
		</div>
      </div>
      <div class="bottom-answer-text-block">
		<div class="droppable base" var:base="1">
			<div class="info-text-box" var:id="5">
				<div class="draggable item" var:id="5">
				      Идея производства «точно вовремя»
				</div>
			</div>
			<div class="info-text-box" var:id="2">
				<div class="draggable item" var:id="2">
				      Стандартизация деталей и механизмов
				</div>
			</div>
			<div class="info-text-box big" var:id="4">
				<div class="draggable item" var:id="4">	
				      Окончательное формирование системы массового производства
				</div>
			</div>
			<div class="info-text-box bigest">
				<div class="inner-small-info-box" var:id="0">
					<div class="draggable item" var:id="0">
					      «4 принципа научного менеджмента»
					</div>
				</div>
				<div class="inner-small-info-box small" var:id="7">
					<div class="draggable item small" var:id="7">
					      Вовлечение людей
					</div>
				</div>
				<div class="inner-small-info-box" var:id="6">
					<div class="draggable item" var:id="6">	
					      Первые шаги в формировании TPS
					</div>
				</div>
			</div>
			<div class="info-text-box up-top-second" var:id="1">
				<div class="draggable item" var:id="1">	
				      Принцип <br />автономизации
				</div>
			</div>
			<div class="info-text-box up-top-second" var:id="8">
				<div class="draggable item" var:id="8">
				      Научная <br />организация труда
				</div>
			</div>
			<div class="info-text-box up-top" var:id="3">
				<div class="draggable item" var:id="3">
				      Стандартизация операций рабочих
				</div>
			</div>
			<div class="info-text-box">
			      
			</div>
			<div style="clear:both"></div>
		</div>
	</div>
</form>
</div>
<style>
.draggable{
	cursor: move;
}
.draggable:hover{
	opacity: .7;
}
</style>