<?php  
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Layout
{
    
    public $obj;
    public $layout;
    public $theme = '';
    
    public $scripts = array();
    public $styles = array();
    public $title = array();
    protected $controller;
    
    function Layout( $params )
    {
        $layout = (isset($params['layout'])) ? 'layouts/'.$params['layout'] : 'layouts/layout_main';
        $controller = $params['controller'];
        
        $this->controller = $controller;
        $this->obj =& get_instance();
        if(defined('THEME'))
        {
            $this->theme = THEME.'/';
        }
        $this->layout = $this->theme.$layout;
    }

    function setLayout($layout)
    {
        if(defined('THEME'))
        {
            $this->theme = THEME.'/';
        }
        $this->layout = $this->theme.$layout;
    }
    
    function view($view, $data=null, $return=false)
    {
        $view = $this->theme.$view;
        $loadedData = array();
        $loadedData['content_for_layout'] = $this->obj->load->view($view,$data,true);
        $data = (array)$data;
        $loadedData = array_merge($loadedData, $data);
        //print_r($this->controller->config);
        //echo $this->controller->config->item('static_url');
        $static_url = $this->controller->config->item('base_url').$this->controller->config->item('static_url').'/'.$this->theme;
        foreach($this->scripts as $key => $val)
        {
            $this->scripts[$key] = $static_url.'js/'.$this->scripts[$key];
        }
        foreach($this->styles as $key => $val)
        {
            $this->styles[$key] = $static_url.'css/'.$this->styles[$key];
        }
        $loadedData['static_url'] = $static_url;
        $loadedData['scripts'] = $this->scripts;
        $loadedData['styles'] = $this->styles;
        $loadedData['title'] = $this->title;
        
        if($return)
        {
            $output = $this->obj->load->view($this->layout, $loadedData, true);
            return $output;
        }
        else
        {
            $this->obj->load->view($this->layout, $loadedData, false);
        }
    }
}
?>